# AppUtil.js

## AppNavigatorBase
### Define Routes
Create a file named _AppNavigator.js_ which extends from AppNavigatorBase. You can define routes in this class.

Example for route "lobby":

```JavaScript
import {AppNavigatorBase} from "./apputil.js";

export class AppNavigator extends AppNavigatorBase {
    lobby = () => this.navigate("lobby");
    isLobby = () => this.isPage("lobby");
}

export const appNavigator = new AppNavigator();
```


#### Define Parameters
Example for route "details" with parameter "id" and "title": 
```JavaScript
    details = (id,title) => this.navigate("details?id="+id+"&title="+title);
    isDetails = () => this.isPage("details");
```

### Usage
#### Import
Import your created _AppNavigator.js_ file.
```JavaScript
import {appNavigator} from "./AppNavigator.js";
```
#### Navigate to Page
Navigation to page is initiated via the defined method `details(id,title)` from _AppNavigator.js_
```JavaScript
appNavigator.details(1, "My Title")
```
#### Check Current Page
```JavaScript
if(appNavigator.isDetails()){
    render('<h1>Details</h1>');
}
```
#### Get Parameter
```JavaScript
if(appNavigator.isDetails()){
    let id = navigator.getParam("id"); // "1"
    let title = navigator.getParam("title"); // "My Title"
    render(`<h1>Details ${title} + ${id}</h1>`);
}
```
#### Register Navigation Change Listener (Svelte)
```JavaScript
let navigate = appNavigator;
appNavigator.onChange(()=> navigate = navigate);
```