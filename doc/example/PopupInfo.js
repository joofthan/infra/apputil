import {ShadowElement} from "../../apputil/apputil.js";

class PopUpInfo extends ShadowElement {
    connectedCallback(){
        this.render(`
            hello <b>${this.attr('myattr')}</b>
            <p></p>
        `);
        let p = this.el('p');
        p.innerHTML='gutem morgen';
    }
}
customElements.define('popup-info', PopUpInfo);