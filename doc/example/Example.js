import {AppElement, ShadowElement} from "../../apputil/apputil.js";
import "./MyButton.js";
import "./PopupInfo.js";

class Documentation extends ShadowElement{
    connectedCallback(){
        document.title = 'Example';
        this.render(`
<link rel="stylesheet" type="text/css" href="../apputil/autostyle.css">
<style>
img{
width: 100%;
}
</style>
<div class="autostyle">
                <h1>Examples</h1>
                <h2>Custom Elements</h2>
                
                <popup-info img="img/alt.png" myattr="My Attribute Text" ></popup-info>
                <br><br>
                <input type="text" id="documentation_input">
                <my-button id="documentation_button" text="Do some Action"></my-button>
                
                <my-button text="Do some Action"></my-button>
                
                <br>
                <h2>Button</h2>
                <code>&lt;button&gt;</code>
                <br><br>
                <button type="button">
                    Add to favorites
                </button>
                
                <br><br>
                <h2>Input Text</h2>
                <code>&lt;input type="text"&gt;</code>
                <br><br>
                <label for="name">Name (4 to 8 characters):</label>
                <input id="name" maxlength="8" minlength="4" name="name"
                       placeholder="input text" required size="10" type="text">
                
                <br><br>
                <h2>Checkbox</h2>
                <code>&lt;input type="checkbox" checked&gt;</code>
                <br><br>
                <input checked id="scales" name="scales"
                       type="checkbox">
                <label for="scales">Scales</label>
                
                <br><br>
                <h2>Datepicker</h2>
                <code>&lt;input type="date"&gt;</code>
                <br><br>
                <label for="start">Start date:</label>
                <input id="start" max="2018-12-31" min="2018-01-01"
                       name="trip-start"
                       type="date" value="2018-07-22">

                <img src="http://joofthan.com/photowebsite/images/freiburg/IMAG0373.jpg">
                <img src="http://joofthan.com/photowebsite/images/freiburg/IMAG0359.jpg">
                <img src="http://joofthan.com/photowebsite/images/freiburg/IMAG0363.jpg">
            </div>
        `);

        let textField = this.input('#documentation_input');
        textField.addEventListener('keyup', ()=>this.button('my-button').setAttribute('text', textField.value));
    }
}

customElements.define('app-example', Documentation);
