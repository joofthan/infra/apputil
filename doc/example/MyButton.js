import {AppElement} from "../../apputil/apputil.js";

class MyButton extends AppElement{
    connectedCallback(){
        this.render(`
                <button>${this.attr('text')}</button>
        `);
    }

    static get observedAttributes() {return ['text']; }
    attributeChangedCallback(name, oldValue, newValue) {
        this.connectedCallback();
    }
}

customElements.define('my-button', MyButton);