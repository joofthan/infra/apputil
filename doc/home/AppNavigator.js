import {AppNavigatorBase} from "../../apputil/apputil.js";

export class AppNavigator extends AppNavigatorBase {
    documentation = () => this.navigate("documentation");
    isDocumentation = () => this.isPage("documentation");
    example = () => this.navigate("example");
    isExample = () => this.isPage("example");
}

export const appNavigator = new AppNavigator();