import {AppElement, ShadowElement} from "../../apputil/apputil.js";
import "../dokumentation/Dokumentation.js";
import "../example/Example.js";
import "../readme/Readme.js";
import {appNavigator} from "./AppNavigator.js";

class AppMain extends ShadowElement{
    constructor() {
        super();
        appNavigator.onChange(()=>this.view());
    }

    connectedCallback(){
        this.view();
    }

    view(){
        let mainContent = null;
        if(appNavigator.isHome()){
            mainContent = `<app-readme/>`;
        }else if(appNavigator.isDocumentation()){
            mainContent = `<app-documentation/>`;
        }else if(appNavigator.isExample()){
            mainContent = `<app-example/>`;
        } else {
            mainContent = `Route "${appNavigator.page}" not found `;
        }

        let content = `
                        <link rel="stylesheet" type="text/css" href="../apputil/autostyle.css">
                        <style>
                        .grid{
                        display: grid; grid-template-columns: 15em 1fr; gap:1em;
                        }
                        a {
                        cursor: pointer;
                        }
                        </style>
                        <div class="grid autostyle">
                            <div style="width: 15em; border: solid black 1px; justify-content: center; ">
                                <h2 style="text-align: center;">Menue</h2>
                                <ul>
                                    <li><a id="home_link">Readme</a></li>
                                    <li><a id="documentation_link" >Documentation</a></li>
                                    <li><a id="example_link">Example</a></li>
                                </ul>
                            </div>
                            <div>
                                 ${mainContent}               
                            </div>
                       </div>`;

        this.render(content);
        this.el('a#home_link').addEventListener('click', ()=> appNavigator.home());
        this.el('a#documentation_link').addEventListener('click', ()=> appNavigator.documentation());
        this.el('a#example_link').addEventListener('click', ()=> appNavigator.example());
    }
}
customElements.define('app-main', AppMain);
