import {AppElement} from "../../apputil/apputil.js";

class Readme extends AppElement{
    connectedCallback(){
        this.view();
    }

    async view() {
        document.title = 'Readme';
        let readmeContent = await fetch('/README.md').then(e => e.ok?e.text():alert('Error loading readme'));
        this.render(`
                <h1>Readme</h1>
                <p></p>
        `);
        readmeContent = readmeContent.replaceAll('<', '&lt;')
                 .replaceAll('>', '&gt;')
                 .replaceAll('\n', '<br>');
        this.el('p').innerHTML = readmeContent;
    }
}

customElements.define('app-readme', Readme);
