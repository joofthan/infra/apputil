import {AppElement} from "../../apputil/apputil.js";

class Documentation extends AppElement{
    connectedCallback(){
        document.title = 'Documentation';
        this.render(`
                <h1>Web Components</h1>
        <ul>
            <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/Web_Components">Overview</a></li>
            <li><a target="_blank"
                   href="https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements">Custom
                Elements</a> - <a target="_blank"
                                  href="https://github.com/mdn/web-components-examples/blob/master/life-cycle-callbacks/main.js">Example</a>
            </li>
            <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM">Shadow
                DOM</a></li>
            <li><a target="_blank"
                   href="https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_templates_and_slots">Template
                and Slot</a></li>
        </ul>
        <h2>HTML</h2>
        <ul>
            <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table">Table</a></li>
            <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element">Element Overview</a>
                <ul>
                    <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element#forms">Form-Elements</a>
                    </li>
                    <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input">Input-Elements</a>
                    </li>
                </ul>
            </li>
        </ul>
        <h2>JavaScript</h2>
        <ul>
            <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes">Classes</a>
            </li>
            <li><a target="_blank"
                   href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules">Module</a> (using <a
                    target="_blank"
                    href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode">Strict Mode</a>)
                <ul>
                    <li><a target="_blank"
                           href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import">Import</a>
                    </li>
                    <li><a target="_blank"
                           href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export">Export</a>
                    </li>
                </ul>
            </li>

            <li>Intellisense with <a target="_blank" href="https://jsdoc.app/index.html">JSDoc</a>
                <ul>
                    <li><a target="_blank" href="https://jsdoc.app/tags-returns.html">Return Types</a></li>
                    <li><a target="_blank" href="https://jsdoc.app/tags-param.html">Param Types</a></li>
                </ul>
            </li>
        </ul>

        <h2>CSS</h2>
        <ul>
            <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Grids">Grid
                Layout</a> - <a target="_blank"
                                href="https://css-tricks.com/snippets/css/complete-guide-grid/">Example</a></li>
            <li><a target="_blank"
                   href="https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox">Flexbox</a> - <a
                    target="_blank" href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">Example</a></li>
            <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties">Variables</a>
                - <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/CSS/--*">Example</a></li>
            <li>Design
                <ul>
                    <li><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/CSS/box-shadow">Shadow</a>
                        / <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-shadow">Text-Shadow</a>
                    </li>
                    <li><a target="_blank"
                           href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Using_CSS_animations">Animation</a>
                    </li>
                    <li><a target="_blank"
                           href="https://developer.mozilla.org/en-US/docs/Web/CSS/transition">Transition</a></li>
                </ul>
            </li>
        </ul>
        <br>
        <h3>Resources</h3>
        Video: <a target="_blank" href="https://youtu.be/3elGSZSWTbM">Grid vs Flexbox</a> <br>
        <br>
        TODO: <br>
        - Fetch API/RestClient 
        `);
    }
}

customElements.define('app-documentation', Documentation);
